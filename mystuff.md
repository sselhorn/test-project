edit to trigger pipeline

anotehr edit

# What I'm working on


## 12.10 release posts Jackie:

### DONE!

DONE: Multiple release evidence: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/44303

- **Related doc:** Sean wrote about using API for on-demand evidence: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/26760
- We went with collect release evidence. 
  
DONE: Executable runbooks (links in release assets): https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/41001

- https://docs.gitlab.com/ee/user/project/releases/#release-assets  (Links topic)

### In progress

Moved to 13: Test results in release evidence: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/44318

- Use GitLab issues for test case management and reporting from CI
  Use CI job to iterate test report JSON (to generate list of test cases that were executed, and their results)
  https://gitlab.com/gitlab-org/gitlab/-/issues/32773
  No work done yet, but it says it's high priority. No docs link/docs link incorrect.

DOCS IN PROGRESS!: JWT Auth for Vault: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/44313

- Docs here! https://gitlab.com/gitlab-org/gitlab/-/merge_requests/28063

NO DOCS: Deploy freeze: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/44306

- Rayana has some designs. Neat. https://gitlab.com/gitlab-org/gitlab/-/issues/24295/designs
- If the jobs are run while the deploy freeze is happening, they will be skipped and have to be restarted when time is up. 


NO DOCS: Quick assign commenter button: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/41058

- The feature MR is likely done but the docs are not. Feature MR: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/23883

NO DOCS: Auto-changelog: ?? https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/40997

- This is something about creating release notes from annotated tags. The feature work doesn't seem slated for 12.10. 
- (No work has been done, feature issue is on backlog.)

## 12.10 release post deprecation Jackie:

- Deprecation release evidence: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/43970

## 12.10 release posts Orit:

NO DOCS: Quick action for Merge Trains: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/44483

- I did brief edit but the feature is not ready yet... Docs need to be updated (https://docs.gitlab.com/ee/user/project/quick_actions.html)

NO DOCS: Blue-green deployments: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/44481/

- This is a docs-only thing...

NO DOCS: Feature flag strategies API: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/43398

- I added some comments but docs are not done and this is hidden behind feature flag...
- I unassigned myself because what can I do about no docs?
  
Feature flag strategies in UI: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/43404

Allow Deploy keys to push to protected branches: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/44839/
  
- They are still discussing.

Official docker image for deployment to ECS: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/44879

Predefined AWS variables: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/42699


## Other MRs:

- handbook update about overview heading: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/28828
- Very small Etienne deprecation message: https://gitlab.com/gitlab-org/cloud-deploy/-/merge_requests/8
- Nathan edit assets (small update): https://gitlab.com/gitlab-org/gitlab/-/merge_requests/28818
- JWT: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/28063
- Vladimir content for Let's Encrypt: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/28642
- Community MR about bug, but Nathan is going to fix: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/28319
- Update spacing on docs site: https://gitlab.com/gitlab-org/gitlab-docs/-/merge_requests/766
- Get rid of Overview heading in topic example: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/28828
- UI text for tracking release assets: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/26821
- Pipelines for merge requests how to configure: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/28374
- Marcel inline links: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/28391 https://gitlab.com/gitlab-org/gitlab/-/merge_requests/28389
- community contribution for a variable: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/28059
- UI text for will be created in 1 month, etc for releases: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/28075
- Fix code blocks: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/28065
- On-demand release evidence: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/26760
- My proudest MR about merge trains, pipelines, etc: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/27255
- Small MR for Etienne about AWS ECS template: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/27382
- Community contribution about runner: https://gitlab.com/gitlab-org/gitlab-runner/-/merge_requests/1946
- Small edit from Hendrik Meyer (random): https://gitlab.com/gitlab-org/gitlab/-/merge_requests/28007


## Other issues:

- UI text/messages: They are planning to deprecate the old way of deprecating feature flags: https://gitlab.com/gitlab-org/gitlab/-/issues/213369
- Feature flag for release evidence--can remove? https://gitlab.com/gitlab-org/gitlab/-/issues/213132
- This one about merge requests checkbox that is not clear: https://gitlab.com/gitlab-org/gitlab/-/issues/29207
- I am trying to get the check box text updated for merge trains and pipelines for merged results: https://gitlab.com/gitlab-org/gitlab/-/issues/212875
- Pipelines for merge requests how to configure: https://gitlab.com/gitlab-org/gitlab/-/issues/198038
- Create issue when someone rolls out feature flag 100%: https://gitlab.com/gitlab-org/gitlab/-/issues/212454
- Assigned back to Marcel: Environment variable precedence and ordering: https://gitlab.com/gitlab-org/gitlab/-/issues/35066
- Move merge train docs folders: https://gitlab.com/gitlab-org/gitlab/-/issues/212501
- UI text for retry outdated jobs: https://gitlab.com/gitlab-org/gitlab/-/issues/211339
- UX definition of done (DoD): https://gitlab.com/gitlab-org/ci-cd/release-management-group/release-management/-/issues/21


## My OKR:

- Nav updates: https://gitlab.com/gitlab-org/gitlab-docs/-/merge_requests/764/
- Small style/standardization issues: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/28685

Gatsby project template: https://gitlab.com/gitlab-org/gitlab/-/merge_requests/27538  
Do we have to do anything or link anywhere?

Here's the issue about the "change path" step being unclear: https://gitlab.com/gitlab-org/gitlab/-/issues/209671
Has the two linked MRs.

My CI questions: https://docs.google.com/document/d/1RBLMNe1ohTDTLvbPbMvuJxxy8dcU5qUJbpD2WISMoxo/edit#heading=h.vyfxzha9u02j
